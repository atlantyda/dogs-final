import React from 'react';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import { Redirect } from 'react-router-dom';

const style = {
  button: {
    width: 160,
    height: 50,
    borderRadius: 4,
    color: "white",
    fontFamily: "Rubik, sans-serif",
    marginLeft: 30,
    transform: "translateY(-8px)"
  },
  label: {
    letterSpacing: 1,
  },  
  input: {
    fontFamily: "Rubik, sans-serif",
    fontSize: 20
  },
  textField: {
    height: 80,
  },
  floating: {
    fontFamily: "Rubik, sans-serif",
    fontSize: 20,
    color: 'lightgray',
  },
  underline: {
    borderColor: '#be14e0',
  }
}

class MuttSearch extends React.Component {
  constructor() {
    super();
    this.state = {
      breed: '',
      breedquery: '',
      searching: window.location.pathname !== "/"
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKey = this.handleKey.bind(this);
  }
  handleChange(e) {
    this.setState({breed: e.target.value.toLowerCase().replace(/[^a-z ]/, ''), breedquery: ''});
  }
  handleClick(e) {
    const breed = this.state.breed.trim().replace(/\s+/, '-');
    if (breed) {
      this.setState({searching: true, breedquery: breed});
    }
  }
  handleKey(e) {
    if (e.key === 'Enter') {
      this.handleClick();
    }
  }
  render() {
    return (      
      <div className={this.state.searching ? "sidesearch" : "centresearch"} >
        <TextField
          floatingLabelText="Breed name"
          onChange={this.handleChange}
          onKeyPress={this.handleKey}
          style={style.textField}
          inputStyle={style.input}
          floatingLabelStyle={style.floating}
          underlineStyle={style.underline}
          underlineFocusStyle={style.underline}
          value={this.state.breed}
        />
        <FlatButton
          onClick={this.handleClick}
          backgroundColor="#3733ff" 
          style={style.button}
          label="SEARCH"
          labelStyle={style.label}
        />
        {this.state.breedquery && <Redirect to={'/' + this.state.breedquery} />}
      </div>
    );
  }
}

export default MuttSearch;
