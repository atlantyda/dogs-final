import React, { Fragment } from 'react';
import Pictures from './Pictures.js';
import FullPicture from './FullPicture.js';

class PictureContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      picture: '',
      pictureVisible: false
    };
    this.handleImgClick = this.handleImgClick.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  handleImgClick(src) {
    this.setState({pictureVisible: true, picture: src});
  }
  closeModal() {
    this.setState({pictureVisible: false});
    setTimeout(() => this.setState(
      (prevState) => prevState.pictureVisible === false && {picture: ''}
    ), 300);
  }
  render() {
    return <Fragment>      
      <Pictures breed={this.props.match.params.breed} onImgClick={this.handleImgClick} />
      {this.state.picture &&
        <FullPicture src={this.state.picture} onClick={this.closeModal}
          closing={!this.state.pictureVisible} />
      }
    </Fragment>
  }
}

export default PictureContainer;