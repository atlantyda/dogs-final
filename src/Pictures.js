import React from 'react';
import LazyLoad from 'react-lazy-load';
import DidntFind from './DidntFind.js';
import RefreshIndicator from 'material-ui/RefreshIndicator';

function filenameFromPath(path) {
  return path.substring(path.lastIndexOf('/') + 1, path.length - 4);
}

function ImgBg(props) {
  return (
    <div className="imgbg"
      onClick={(e) => props.onClick(props.src)}
      style={{backgroundImage: `url(${props.src})`}} />
  );
}

class Pictures extends React.Component {  
  constructor() {
    super();
    this.state = {list: [], error: false}
  }
  search(breed) {
    if (!breed.match(/[a-z](-[a-z])?/)) {
      this.setState({error: true});
      return;
    }
    let breedPath = '';
    const breedParts = breed.split('-');
    switch(breedParts.length) {
      case 1:
        [breedPath] = breedParts;
        break;
      case 2:
        breedPath = breedParts[1] + '/' + breedParts[0];
        break;
      default:
        this.setState({error: true});
        return;
    }
    fetch(`/api/breed/${breedPath}/images`)
      .then(function(response) {
        return response.json();
      })
      .then(json => {
        if (json.status === 'success') {
          this.setState({error: false, list: json.message})
        } else {
          this.setState({error: true, list: []})
        };
      });
  }
  componentDidMount() {
    this.search(this.props.breed);
  }
  componentWillReceiveProps(nextProps) {
    this.search(nextProps.breed);
  }
  render() {
    if (this.state.error) {
      return <DidntFind />;
    }
    return (      
      <div className="pictures">
        {this.state.list.length ?
          this.state.list.map(img => (
            <LazyLoad height={192} width={212} offset={1000} debounce={false}
              key={filenameFromPath(img)}>
                <ImgBg src={img} onClick={this.props.onImgClick} />
            </LazyLoad>
          ))
        : <RefreshIndicator status="loading"
            size={70} left={0} top={0}
            style={{display: "inline-block", position: "relative"}} />}
      </div>      
    );
  }
}  

export default Pictures;