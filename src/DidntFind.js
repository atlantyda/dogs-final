import React from 'react';

const style = {
  container: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    left: 0,
    top: 0,
    minHeight: 500,
    pointerEvents: 'none',
  },
  msg: {
    fontFamily: 'Rubik, sans-serif',
    fontSize: '20pt',
    position: 'relative',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'inline-block',
  }
}

function DidntFind() {
  return <div style={style.container}>
    <div style={style.msg}>
      I didn't find any pictures.
      <span role="img" aria-label="smile">😔</span>
    </div>
  </div>
}

export default DidntFind;