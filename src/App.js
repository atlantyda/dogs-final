import React, { Fragment } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MuttSearch from './MuttSearch';
import PictureContainer from './PictureContainer.js';
import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
    <MuiThemeProvider>        
      <Router>
        <Fragment>
          <a href="/" id="logo"><span className="violet">Dog</span>App</a>
          <MuttSearch />
          <Route path="/:breed" component={PictureContainer} />
        </Fragment>
      </Router> 
    </MuiThemeProvider>
  );
}
  
export default App;
