import React, { Fragment } from 'react';

class FullPicture extends React.Component {
  constructor(props) {
    super(props);
    this.state = {visible: false};
  }
  componentDidMount() {
    setTimeout(()=>this.setState({visible: true}));
  }
  componentWillReceiveProps(nextProps) {
    this.setState({visible: !nextProps.closing});
  }
  render() {
    return (
      <Fragment>
        <div className={"modal " + (this.state.visible ? "visible" : "notvisible")} onClick={this.props.onClick} />
        {this.state.visible &&
          <div className="lightbox">
            <div className="material-icons"><span onClick={this.props.onClick}>close</span></div>
            <img className="full" src={this.props.src} alt="" />
          </div>}
      </Fragment>
    );
  }
}

export default FullPicture;